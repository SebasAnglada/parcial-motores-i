using UnityEngine;
using UnityEngine.UI;

public class Victoria : MonoBehaviour
{
    public Text victoryText;
    public GameObject Timer;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            victoryText.color = new Color(victoryText.color.r, victoryText.color.g, victoryText.color.b, 1.0f);
            Destroy(gameObject);
            Destroy(Timer.gameObject);
        }
    }
}
