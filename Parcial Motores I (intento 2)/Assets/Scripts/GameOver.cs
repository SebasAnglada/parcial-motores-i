using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text gameOverText; 
    public Timer timer;

    private bool gameIsOver = false;

    void Update()
    {
        if (timer.timer <= 0 && !gameIsOver)
        {
            gameIsOver = true;
            Time.timeScale = 0;
            gameOverText.enabled = true;
            gameOverText.color = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 1.0f);
        }
    }
}
