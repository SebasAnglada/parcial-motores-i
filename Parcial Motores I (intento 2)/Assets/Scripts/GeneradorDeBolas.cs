using System.Collections;
using UnityEngine;

public class GeneradorDeBolas : MonoBehaviour
{
    public GameObject Bola;
    public Transform spawnPoint;
    public float timeBetweenSpawns = 1.0f;

    private float nextSpawnTime;

    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnBall();
            nextSpawnTime = Time.time + timeBetweenSpawns;
        }
    }

    void SpawnBall()
    {
        GameObject newBall = Instantiate(Bola, spawnPoint.position, Quaternion.identity);
        Rigidbody rb = newBall.GetComponent<Rigidbody>();

        if (rb != null)
        {
            rb.AddForce(Vector3.down * 10f, ForceMode.Impulse);
        }
        Destroy(newBall,5f); 
    }
}
