using UnityEngine;

public class SeguirCamara : MonoBehaviour
{
    public Camera mainCamera;

    private void Update()
    {
        if (mainCamera != null)
        {
            transform.position = mainCamera.WorldToScreenPoint(Vector3.zero);
        }
    }
}
