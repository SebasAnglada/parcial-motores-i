using UnityEngine;

public class RotatingPusher : MonoBehaviour
{
    public float rotationSpeed = 50.0f; 
    public float pushForce = 10.0f; 

    private Vector3 initialRotation;

    void Start()
    {
        initialRotation = transform.rotation.eulerAngles;
    }

    void Update()
    {
        transform.Rotate(Vector3.left * rotationSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
                Vector3 forceDirection = collision.contacts[0].point - transform.position;

                transform.rotation = Quaternion.Euler(initialRotation);
        }
    }
}
