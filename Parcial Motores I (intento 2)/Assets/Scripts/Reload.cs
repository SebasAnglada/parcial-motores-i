using UnityEngine;
using UnityEngine.SceneManagement;

public class Reload : MonoBehaviour
{
    public Transform Jugador;

    void Update()
    {
        restart();
    }

    public void restart()
    {
        if ((Input.GetKeyDown(KeyCode.R)) || (Jugador.position.y <= -20))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
    }
}
