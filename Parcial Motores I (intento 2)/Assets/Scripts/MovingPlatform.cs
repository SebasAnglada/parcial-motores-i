using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float upDownDistance = 4.0f; // Total distance platform should move.
    public float moveSpeed = 2.0f;

    private Vector3 initialPosition;
    private bool movingUp = true;

    void Start()
    {
        initialPosition = transform.position;
    }

    void Update()
    {
        if (movingUp)
        {
            transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
            if (transform.position.y >= initialPosition.y + upDownDistance)
                movingUp = false;
        }
        else
        {
            transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
            if (transform.position.y <= initialPosition.y)
                movingUp = true;
        }
    }
}
