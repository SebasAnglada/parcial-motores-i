using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class PowerUp : MonoBehaviour
{
    float multiplicador = 2.0f;

    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Jugador"))
        { 
            pickup(other);
        }
    }

    void pickup(Collider jugador)
    {

        ControlJugador stats =  jugador.GetComponent<ControlJugador>();   
        stats.rapidezDesplazamiento *= multiplicador; 

        Destroy(gameObject);
    }

}