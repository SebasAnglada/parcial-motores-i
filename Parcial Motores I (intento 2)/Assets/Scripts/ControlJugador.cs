using UnityEngine;
using System.Collections;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float jumpForce = 10.0f;
    private Rigidbody rb;
    private bool isGrounded;
    private int totalDeSaltos = 2; 
    private int saltosRestantes;
    public Camera MainCamera;
    public GameObject Proyectil;
    public int pushForce;
 

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        saltosRestantes = totalDeSaltos; 
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetButtonDown("Jump") && saltosRestantes > 0){
            Saltar();
        }

        if (Input.GetMouseButtonDown(0))
   {
         Ray ray = MainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

         GameObject pro;
         pro = Instantiate(Proyectil, ray.origin, transform.rotation);

         Rigidbody rb = pro.GetComponent<Rigidbody>();
         rb.AddForce(MainCamera.transform.forward * 15, ForceMode.Impulse);

         Destroy(pro, 5);
   }
    }
    
    void Saltar()
    {
    rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    saltosRestantes--;  
    }

    void OnCollisionEnter(Collision collision){
        if (collision.gameObject.CompareTag("Piso")){
            saltosRestantes = totalDeSaltos; 
        }
        if (collision.gameObject.CompareTag("Ball") || collision.gameObject.CompareTag("Bot"))
        {
            Rigidbody rb = GetComponent<Rigidbody>();

            if (rb != null)
            {
                Vector3 forceDirection = collision.contacts[0].point - transform.position;
                rb.AddForce(forceDirection.normalized * pushForce, ForceMode.Impulse);
            }
        }
    }
}
