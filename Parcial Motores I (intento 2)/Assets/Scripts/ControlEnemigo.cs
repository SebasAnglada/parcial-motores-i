using UnityEngine;

public class ControlEnemigo : MonoBehaviour
{
    public int hp = 100;
    public Transform Jugador;
    public GameObject ProyectilEnemigo;
    public float intervaloDeDisparo = 2.0f;
    public float velocidadProyectil = 5.0f;
    private float proximoDisparo;
    public int rapidez;
    private Rigidbody rb;

    public float detectionRadius = 5f;
    private bool playerDetected = false;


    void start()
    {
        hp = 100;
        rb = GetComponent<Rigidbody>();
        Jugador = GameObject.Find("Jugador").transform;
    }   

    public void recibirDano(){
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);    
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            recibirDano();
        }
    }
    
    void Update()
    {

        float distanceToPlayer = Vector3.Distance(transform.position, Jugador.position);

        if (distanceToPlayer < detectionRadius)
        {
            
            playerDetected = true;
            Debug.Log("Player detected!");
            if (playerDetected == true)
            {
            Disparar();
            transform.LookAt(Jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
            }
        }
        
        }
        void Disparar()
        {
            
        if(Time.time >= proximoDisparo){
        proximoDisparo = Time.time + intervaloDeDisparo;
        GameObject Proyectiles = Instantiate(ProyectilEnemigo, transform.position, Quaternion.identity);
        Rigidbody rb = Proyectiles.GetComponent<Rigidbody>();

        Vector3 shootDirection = (Jugador.position - transform.position).normalized;

        rb.velocity = shootDirection * velocidadProyectil;

        Destroy(Proyectiles, 3.0f);
        }
        }
}
