using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DesaparecerTexto : MonoBehaviour
{
    public GameObject Objetivo;
    
    void Start()
    {
        destroyDelayed();
    }

    void destroyDelayed()
    {
        Destroy(Objetivo,3);
    
    }

    
}
